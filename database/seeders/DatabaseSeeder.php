<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        $categories = 
        [
            ['name'=> 'Motorbike','icon' => 'public/img/M-1.png'],
            ['name'=> 'Fornnitures','icon' => 'public/img/F-1.png'],
            ['name'=> 'Elettronic','icon' => 'public/img/F-7.png'],
            ['name'=> 'Cars','icon' => 'public/img/C-1.png'],
            ['name'=> 'Games','icon' => 'public/img/G-1.png'],
            ['name'=> 'Houses','icon' => 'public/img/H-2.png'],
            ['name'=> 'Informatics','icon' => 'public/img/I-1.png'],
            ['name'=> 'Phones','icon' => 'public/img/P-6.png'],
            ['name'=> 'Sport','icon' => 'public/img/S-5.png'],
        ];
         foreach($categories as $category){
            Category::create([
                'name'=>$category['name'],
                'icon'=>$category['icon'],
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
        }
    }
}
