<x-layout>
    <div class="container vh-100 d-flex align-items-center ">
    @if ($announcement_to_check)
        <div class="row  ">
           


  <!-- Modal -->
  <div class="modal fade custom-fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen  ">
      <div class="modal-content custom-content ">
        <div class="modal-header border border-0 ">
        </div>
        <div class="modal-body d-flex align-items-center ">
          


            <div class="col-lg-12 col-md-12  p-0  d-flex align-items-center d-flex justify-content-center   ">
                
                <div id="carouselExampleControls" class="carousel slide  " data-bs-ride="carousel">
                    @if($announcement_to_check->images)
                    <div class="carousel-inner">
                        @foreach($announcement_to_check->images as $image)
                        <div class="carousel-item   @if($loop->first)active @endif">


                            {{-- <img class="img-fluid custom-border-l" src="{{Storage::url($image->path)}}" class="d-block w-100   " alt="..."> --}}
                            <img class="img-fluid"  src="{{$image->getUrl(750, 500)}}"> 

                            {{-- <img class="img-fluid custom-border-l" src="{{Storage::url($image->path)}}" class="d-block w-100 mt-4 mb-4  " alt="..."> --}}
                           {{-- <img class="img-fluid" src="{{$image->getUrl(800, 500)}}"> --}}

                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="carousel-inner">
                        <div class="carousel-item">
                            <img src="https://picsum.photos/id/27/1200/200" class="d-block w-100 custom-dim rounded " alt="...">

                            <img src="https://picsum.photos/id/27/1200/200" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/id/27/1200/200" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    @endif
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="next">
                <span class="carousel-control-next-icon " aria-hidden="true"></span>
                <span class="visually-hidden ">Next</span>
            </button>
        </div>
    </div>



        </div> 
        <div class="modal-footer">
          <button type="button" class="btn custom-btn-modal custom-btn-center" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-7 d-flex align-items-center">

   <img class="img-fluid" src="../public/../media/revisor.png" alt="">
  </div>

    <div class="col-lg-4 col-md-12 secondaryBg2  custom-border-r custom-border-l mb-5 ">

      
        <h4 class=" custom-title-ann fw-semibold mx-auto border-bottom">
            {{ $announcement_to_check ? 'Announcement Review' : 'there are no announcement to review' }}
        </h4>
        <div class="row  justify-content-center mt-5">
           
            <div class=" text-center custom-title-style mb-4 ">
                <div class="text-center custom-post-style">
                    <h3 class="card-text ">Title: <span class="custom-font-size border-bottom"> {{ $announcement_to_check->title }} </span> </h3>
                </div>
            </div>
            <h2 class="card-text textWhite text-center ">Description </h2>
            <div class=" text-center  custom-description ">
                <p class="custom-p-style">{{ $announcement_to_check->body }}</p>
            </div>
            <div class="text-center custom-post-style mt-3">
                <h4 class="card-text  ">Published on: <span class="custom-font-size">  {{ $announcement_to_check->created_at->format('d/m/Y') }}</span> </h4>
            </div>
        </div>
        <div class="row  mt-5 mb-4  ">
            <div class="d-flex justify-content-evenly">
                <form  action="{{ route('revisor.accept_announcement', ['announcement' => $announcement_to_check]) }}"{{-- inizio form bottone - 1 --}}
                    method="POST">
                    @csrf
                    @method('PATCH')
                    <button type="submit" class=" shadow button-darkBg"><i class="bi bi-check-lg"></i></button>
                </form>{{-- fine form bottone - 1 --}}
                
                <!-- Button trigger modal Revision-->
                <button type="button" class=" button-darkBg" data-bs-toggle="modal" data-bs-target="#exampleX">
                    <i class="bi bi-google"></i>
                
                </button>

                <!-- Button trigger modal Immagini -->
                <button type="button" class=" button-darkBg" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    <i class="bi bi-images"></i>
                </button>

                </button>

                <form action="{{ route('revisor.reject_announcement', ['announcement' => $announcement_to_check]) }}"{{-- fine form bottone - 2 --}}
                    method="POST">
                    @csrf
                    @method('PATCH')
                    <button type="submit" class=" shadow button-darkBg"><i class="bi bi-x-lg"></i></button>{{-- fine form bottone - 2 --}}
                </form>

            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleX" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header custom-border">
                        <h5 class="modal-title mx-auto " id="exampleModalLabel">Announcement Review</h5>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12 whiteBg d-flex align-items-center text-white ">
                            <div class="row ">
                                @foreach($announcement_to_check->images as $image)
                                <div class="row custom-border mt-4">
                                    <div class=" col-6 text-center textColor  ">
                                        <div>
                                            <h5>Revisione immagini</h5>
                                            <p>Adulti: <span class="{{$image->adult}}"></span></p>
                                            <p>Satira: <span class="{{$image->spoof}}"></span></p>
                                            <p>Medicina: <span class="{{$image->medical}}"></span></p>
                                            <p>Violence: <span class="{{$image->violence}}"></span></p>
                                            <p>Contenuto Ammiccante: <span class="{{$image->racy}}"></span></p>
                                        </div>
                                    </div>
                                    <div class=" col-6 text-center textColor my-auto  ">
                                        <h5>tags</h5>
                                        <div>
                                            @if($image->labels)
                                            @foreach($image->labels as $label)
                                            <p class="d-inline">{{$label}}</p>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mx-auto" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
</div>
</x-layout>
