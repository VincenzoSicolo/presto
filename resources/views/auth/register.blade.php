<x-layout> 
    <div class="container">
        <div class="row vh-100 align-items-center mx-auto ">
            <div class="col-12 col-md-6 col-lg-5  mx-auto ">
                <img class="img-fluid " src="./media/register.png" alt="">
            </div>  
            <div class="col-12 col-md-6 col-lg-5  ">
                <div class="bodyregister mt-4">
                    <div class="containerRegister">
                        <form action="{{ route('register') }}" method="POST">
                            @csrf
                            <div class="formregister"> 
                                <span class="titleregister">Register</span>
                                <form>
                                    <div class="input-field-register">
                                        <input name="name" type="text" placeholder="Enter your Name" required>
                                        <i class="bi bi-person-fill ms-2"></i>
                                    </div>
                                    <div class="input-field-register">
                                        <input name="email" type="text" placeholder="Enter your Email" required>
                                        <i class="bi bi-envelope-fill icon-register ms-2"></i>
                                    </div>
                                    <div class="input-field-register">
                                        <input name="password" id="passwordRegister" id="togglePasswordRegister"  type="password" placeholder="Enter your Password" required>
                                        <i class="bi bi-lock-fill icon-register ms-2"></i>
                                        <i class="bi bi-eye-slash eyesRegister" id="togglePasswordRegister"></i>
                                    </div>
                                    <div class="input-field-register">
                                        <input name="password_confirmation" id="passwordRegisterConfirmation" id="togglePasswordRegisterConfirmation" type="password" placeholder="Enter your Password" required>
                                        <i class="bi bi-lock-fill icon-register ms-2"></i>
                                        <i class="bi bi-eye-slash eyesRegister" id="togglePasswordRegisterConfirmation"></i>
                                    </div>
                                    {{-- <div class="checkbox-text-register">
                                        <div class="checkbox-content-register">
                                            <input type="checkbox" id="logCheck">
                                            <label for="logCheck" class="text">Remember Me</label>
                                        </div>
                                        
                                        <a href=""  class="text">Forgot Password?</a>
                                    </div> --}}
                                    <div class="input-field-register textWhite button">
                                        <input type="submit" value="Register Now">
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
const togglePasswordRegister = document.querySelector("#togglePasswordRegister");
        const passwordRegister = document.querySelector("#passwordRegister");

        togglePasswordRegister.addEventListener("click", function() {
            // toggle the type attribute
            const type = passwordRegister.getAttribute("type") === "password" ? "text" : "password";
            passwordRegister.setAttribute("type", type);
            
            // toggle the icon
            this.classList.toggle("bi-eye");
        });


        const togglePasswordRegisterConfirmation = document.querySelector("#togglePasswordRegisterConfirmation");
        const passwordRegisterConfirmation = document.querySelector("#passwordRegisterConfirmation");

        togglePasswordRegisterConfirmation.addEventListener("click", function() {
            // toggle the type attribute
            const type = passwordRegisterConfirmation.getAttribute("type") === "password" ? "text" : "password";
            passwordRegisterConfirmation.setAttribute("type", type);
            
            // toggle the icon
            this.classList.toggle("bi-eye");
        });


        // prevent form submit
        const form = document.querySelector("form");
        form.addEventListener('submit', function (e) {
            e.preventDefault();
        });
    </script>
</x-layout>
