<x-layout>

    <div class="container-fluid custom-bg-login">

       
        <div class="row vh-100 align-items-center mx-auto  ">

            <div class="col-12 col-md-12 col-lg-6 mx-auto me-3 ">
                <img class="img-fluid" src="./media/login.png" alt="">
            </div>

            <div class="col-12 col-md-12 col-lg-5 ">
    <div class="bodylogin mt-4 ">
        <div class="containerLogin ">
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="formlogin"> 
                    <span class="titlelogin">Login</span>
                    <form action="#">
                        <div class="input-field-login">
                            <input type="text" name="email" placeholder="Enter your Email" required>
                            <i class="bi bi-envelope-fill icon-login ms-2"></i>
                        </div>
                        <div class="input-field-login">
                            <input id="passwordLogin" id="togglePasswordLogin" name="password" type="password" class="passwordLogin" placeholder="Enter your Password" required>
                            <i class="bi bi-lock-fill icon-login ms-2"></i>
                            <i class="bi bi-eye-slash eyesLogin" id="togglePasswordLogin"></i>
                        </div>
                        <div class="checkbox-text-login">
                            <div class="checkbox-content-login">
                                <input type="checkbox" name="remember" id="logCheck">
                                <label for="logCheck" class=" textWhite">Remember Me</label>
                            </div>
                            {{-- Quando è attivo togli commento
                                <a href="" class="text">Forgot Password?</a> --}}
                            </div>
                            <div class="textWhite input-field-login button">
                                <input   type="submit" value="Login Now">
                            </div>
                        </form>
                        <div class="login-singup">
                            <span class="textWhite">Not a member?
                                <a href="{{ route('register') }}" type="submit" class="textWhite singup-text">Singup now</a>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
        <script>
            const togglePasswordLogin = document.querySelector("#togglePasswordLogin");
            const passwordLogin = document.querySelector("#passwordLogin");
            togglePasswordLogin.addEventListener("click", function () {
                // toggle the type attribute
                const type = passwordLogin.getAttribute("type") === "password" ? "text" : "password";
                passwordLogin.setAttribute("type", type);           
                // toggle the icon
                this.classList.toggle("bi-eye");
            });
            // prevent form submit
            const form = document.querySelector("form");
            form.addEventListener('submit', function (e) {
                e.preventDefault();
            });
        </script>
    </x-layout>