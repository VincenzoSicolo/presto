<x-layout>
    <div class="container-fluid">
        <div class="row text-center secondaryBg2 lightText">
            <div class="col-12 p-5">
                <h1 class="display-2">{{$category->name}}</h1>

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row mt-5 mb-5 d-flex justify-content-center">
                <div class="col-12">
                    <div class="row justify-content-center ">
                        @forelse ($category->announcements as $announcement)
                            <div class="col-12 col-md-4 p-4 my-4">
                                <div class="card shadow">
                                    <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : 'https://picsum.photos/200'}}" class=" p-1" alt=" ">
                                        <div class="position-absolute rounded secondaryBg2 ms-1 mt-1 ">
                                   <a href="{{route('announcements.show',compact('announcement'))}}" class=" ms-4 button-white-Card card-link m-1 "><i class="bi bi-arrows-fullscreen"></i></a>
                                   {{-- {{ route('categoryAnnouncement') }}  --}} <a href="{{ route('announcements.index') }}" class="text-decoration-none card-link button-white-Card m-1"> <i class="bi bi-reply"></i></a>
                                    </div>
                                        <div class="row p-0 ">
                                    <div class="col-12 col-md-8 mx-auto items-end ">
                                        <div class="card-body p-0 text-center">
                                            <h5 class="textColor">Title: {{$announcement->title}}</h5>
                                            <p class="textColor"> Price: {{$announcement->price}}</p>
                                        </div>
                                        </div>
                                     </div>
                                        <p class="card-foote textColor text-center"> Pubished on: {{$announcement->created_at->format('d/m/Y')}} - Author: {{$announcement->user->name ?? '' }}</p>
                                </div>
                            </div>

                                @empty
                                <div class="col-12">
                                    <p class="h1"> there are no announcements uder this category:{{$category->name}}</p>
                                    <p class="h2"> Publish one: <a href="{{route('announcements.create')}}" class="btn btn-dark shadow p-2"> New announcement</a> </p>
                                </div>
                                @endforelse

                        </div>

                </div>
            </div>
    </div>

</x-layout>