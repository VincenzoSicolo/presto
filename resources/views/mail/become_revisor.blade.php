<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Presto.it</title>
    <style>

        body{
            background-color:#16273E;
            font-family:Arial, Helvetica, sans-serif;
            justify-content: center;
        }

        .box{
            background-color: white;
            text-align: center;
            border-radius: 10px;
            font-size: 1rem;
            width: 75%;
            position: relative;
            left: 10%;

        }

        .box-detail{

            text-align: center;
            display: flex;
            justify-content: center;
            border-radius: 10px;
            background-color: #fff;
            width: 50%;
            position: relative;
            left: 25%;
            top:20px;

        }

        a{

            text-decoration: none;
            background-color: #fff;
            padding: 5px 30px 5px 30px;
            border-radius: 10px;
            position: relative;
            top: 50px;
            color: #f49819;


        }

        a:hover{

            background-color: #16273E;
        }

        .container{

            position: relative;
            top: 10vh;
        }

        @media screen and (max-width: 390px){

            .box-detail{


                width: 75%;
                position: relative;
                left: 11%;

            }


        }

    </style>
</head>

<body>
    <div class="container">


        <div class="box">
            <h1>An User request to work with us</h1>
        </div>

        <div class="box-detail">

        <div >
            <h2>Those are his details:</h2>
        <p>Name:{{ $user->name }}</p>
        <p>Email:{{ $user->email }}</p>
        <p>If you want to make it revisor click here:</p>

        </div>

        </div>

            <div style="text-align:center; ">
                <a href="{{ route('make.revisor', compact('user')) }}">Make Revisor </a>
            </div>



    </div>
</body>

</html>
