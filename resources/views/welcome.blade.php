<x-layout>
    <div class="container-fluid   secondaryBg">
        <div class="row intro-row vh-100 d-flex  justify-content-center">
            <div class="col-12 col-md-12 col-lg-6 mx-auto intro1 d-flex flex-column justify-content-evenly my-auto p-0 ">
                {{-- <h2 class="textColor text-center me-5">{{__('ui.allAnnouncements')}}</h2> --}}
                {{-- {{ __('ui.introduction') }} --}}
                <h1 id="typing-text" class="textWhite text-center me-5 "> </h1>
                <h3 id="typing-text2" class="textWhite text-center fw-bold "></h3>
                <span class="mx-auto ">
                    <div class="setSearch">
                        <form action="{{ route('announcements.search') }}" method="GET" class="d-flex mb-3" role="search">
                            <input name="searched" class="form-control me-2 form-intro whiteBg p-4 " type="search"
                                placeholder="Search for anything" aria-label="Search">
                            <button class="btn custom-search-btn  px-3" type="submit"><i
                                    class="bi bi-search "></i></button>
                        </form>
                    </div>

                    {{-- <h3 class="textWhite text-center  me-3">{{ __('ui.introduction2') }}</h3> --}}
            </div>
            <div class="col-12 col-md-12 col-lg-6 secondaryBg my-auto">
                <div>
                    <img src="./media/home.png " class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid  p-0 ">
        <div class="row vh-100 p-0 flex-column justify-content-center ">
            <div class="row whiteBg p-0 carousel-row my-auto">
                <h2 class="text-center secondary-text my-auto"> {{ __('ui.title-carousel') }}</h2>
                <div class="col-12 col-lg-10 mx-auto ">
                    <div class="swiperBody ">
                        <div class=" slide-container   p-5 swiper swiper-container-free-mode ">
                            <div class="slide-content  " data-swiper-autoplay="2000">
                                <div class="card-wrapper swiper-wrapper  p-0">
                                    @forelse($announcements as $announcement)
                                        @if ($announcement->category)
                                            <div class="card p-1   swiper-slide secondaryBg2   ">
                                                <img class="card-img "src="{{ !$announcement->images()->get()->isEmpty()? $announcement->images()->first()->getUrl(300, 200): 'https://picsum.photos/200' }}"
                                                    alt="">
                                                <div class="position-absolute  rounded  orangeBg ">
                                                    <a href="{{ route('announcements.show', compact('announcement')) }}"
                                                        class="ms-4 secondary-text hover-card card-link m-1 "><i
                                                            class="bi bi-arrows-fullscreen"></i></a>
                                                    <a href="/" class=" card-link secondary-text hover-card m-1"> <i
                                                            class="bi bi-reply"></i></a>
                                                    <a href="{{ route('categoryShow', ['category' => $announcement->category]) }}"
                                                        class="card-link secondary-text hover-card m-1  text-decoration-none">{{ $announcement->category->name }}</a>
                                                </div>
                                                <div class="row p-0 ">
                                                    <div class="col-12 col-md-8 mx-auto items-end">
                                                        <div class="card-body p-0 text-center">
                                                            <h5 class="textWhite"> {{ __('ui.titolo-card') }} :
                                                                {{ $announcement->title }}</h5>
                                                            <p class="textWhite"> {{ __('ui.price') }} :
                                                                {{ $announcement->price }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="card-foote textWhite text-center"> {{ __('ui.publish') }}
                                                    {{ $announcement->created_at->format('d/m/Y') }}
                                                    {{ __('ui.author') }}
                                                    {{ $announcement->user->name ?? '' }}</p>
                                            </div>
                                        @endif
                                    @empty
                                    @endforelse
                                </div>
                                <div class="swiper-button-next secondary-text"></div>
                                <div class="swiper-button-prev secondary-text"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="text-center mb-5">
        <h4 class=" fw-bold  "> Our STAFFS</h4>
    </div> --}}


    <div class="container ">
        <div class="row   align-items-center ">
            {{-- <div class="col">gg --}}
                <div class="text-center secondaryBg textOrange custom-climate  ">
                    <h4 class=" fw-bold  "> Eco Friendly</h4>
                </div>
            <div class="col bgLight2 p-5">
         
                <div class="row">
                 
                    <div class="col-12 div col-md-12 col-lg-4 d-flex justify-content-center mt-5 mb-5">
                        <div class="card flip p-3" style="width: 22rem;">
                            <img src="./media/factory.png" class="card-img-top" alt="...">
                            <div class="card-body ">
                                <h3 class="fs-5 text-center"> {{ __('ui.climate-1') }}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 div col-md-12 col-lg-4 d-flex justify-content-center mt-5 mb-5">
                        <div class="card p-3 flip " style="width: 22rem;">
                            <img src="./media/plant.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h3 class="fs-5 text-center" >{{ __('ui.climate-2') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 div col-md-12 col-lg-4 d-flex justify-content-center mt-5 mb-5">
                        <div class="card p-3 flip" style="width: 22rem;">
                            <img src="./media/recycle.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h3 class="fs-5 text-center"> {{ __('ui.climate-3') }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{-- </div>gg --}}
        </div>
    </div>


            {{-- <div class="text-center mb-3">
                <h4 class=" fw-bold  "> Our STAFFS</h4>
            </div> --}}

            <div class="container-fluid">
                <div class="row align-items-center text-center pb-5 ">
                    <div class="bgLight2 p-5">
                        <div class="row">
                            
                                <div class="text-center secondaryBg textOrange custom-staff mx-auto mb-5">
                                    <h4 class=" fw-bold  "> Our STAFFS</h4>
                                </div>
                            <div class="col-12  div col-md-12 col-lg-4 d-flex justify-content-center mt-5 mb-5">
                                <div class="cShadow secondaryBg2 textWhite overflow-hidden">
                                    <div class="position-relative">
                                        <img class=" img-fluid overflow-hidden" src="./media/vincenzo.png"
                                            alt="">
                                        <div
                                            class="position-absolute start-50  translate-middle d-flex align-items-center">
                                            <a class="btn btnicon mx-1 textWhite" href="https://www.linkedin.com/in/vincenzo-sicolo-3309a9110" target="_blank">
                                                <i class="bi bi-linkedin "></i>
                                            </a>

                                            </a>
                                            <a class="btn btnicon mx-1 textWhite" href="">
                                                <i class="bi bi-instagram "></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="text-center p-4 ">
                                        <h5 class="fw-semibold mb-0 c-third">Vincenzo Sicolo</h5>
                                        <small class="c-main">Designation</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12  div col-md-12 col-lg-4 d-flex justify-content-center mt-5 mb-5">
                                <div class="cShadow secondaryBg2 textWhite  overflow-hidden">
                                    <div class="position-relative">
                                        <img class="img-fluid" src="./media/alexandru.png" alt="">
                                        <div
                                            class="position-absolute start-50 translate-middle d-flex align-items-center">
                                            <a class="btn btnicon mx-1 textWhite" href="https://www.linkedin.com/in/ionut-alexandru-asavinei-263775171/" target="_blank">
                                                <i class="bi bi-linkedin"></i>
                                            </a>

                                            </a>
                                            <a class="btn btnicon mx-1 textWhite" href="">
                                                <i class="bi bi-instagram"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="text-center p-4 ">
                                        <h5 class="fw-semibold mb-0 c-third">Ionut Alexandru Asavinei</h5>
                                        <small class="c-main">Designation</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12  div col-md-12 col-lg-4 d-flex justify-content-center mt-5 mb-5">
                                <div class="  cShadow secondaryBg2 textWhite overflow-hidden">
                                    <div class="position-relative">
                                        <img class="img-fluid immagini-team" src="./media/andrea.png"
                                            alt="">
                                        <div
                                            class="position-absolute start-50  translate-middle d-flex align-items-center">
                                            <a class="btn btnicon mx-1 textWhite" href="https://www.linkedin.com/in/andrea-bruciati-562a0b238/" target="_blank">
                                                <i class="bi bi-linkedin"></i>
                                            </a>

                                            </a>
                                            <a class="btn btnicon mx-1 textWhite" href="">
                                                <i class="bi bi-instagram"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="text-center p-4 ">
                                        <h5 class="fw-semibold mb-0 c-third">Bruciati Andrea</h5>
                                        <small class="c-main">Designation</small>
                                    </div>
                                </div>
                            </div>
                        

                        </div>
                    </div>
                </div>
            </div>






            <script>
                let i = 0,
                    text;

                text = "{{ __('ui.welcome') }}";

                function typing() {
                    if (i < text.length) {
                        document.getElementById("typing-text").innerHTML += text.charAt(i);
                        i++;
                        setTimeout(typing, 70);
                    }

                }

                typing();

                let i2 = 0,
                    text2;

                text2 = "{{ __('ui.welcome-message') }}";


                function typing2() {
                    if (i2 < text2.length) {
                        document.getElementById("typing-text2").innerHTML += text2.charAt(i2);
                        i2++;
                        setTimeout(typing2, 70);
                    }

                }
                typing2();
            </script>

</x-layout>
