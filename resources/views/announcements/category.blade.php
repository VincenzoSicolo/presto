<x-layout>
    
      <div class="container-fluid custom-margin-c  ">
        <div class="row gap-2 d-flex justify-content-center  mt-5 ">
            @foreach ($categories as $category)
                <div class="col-lg-3 secondaryBg col-category">
                    <a href="{{ route('categoryShow', compact('category')) }}" class="category-name2"><h5 class=" category-name2">{{$category->name}}</h5><img class="img-fluid " src="{{ Storage::url($category->icon) }}" alt=""></a>
                </div>
            @endforeach
        </div>
    </div> 
    

</x-layout>
{{-- row-cols-lg-5 row-cols-md-3 row-cols-1 g-2 --}}
