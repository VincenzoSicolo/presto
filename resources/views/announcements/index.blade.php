<x-layout>
    <div class="container">
        <div class="row mt-5">
            <div class="col-12">
                <div class="row">
                
            
                    @forelse ($announcements as $announcement)
                    <div class="col-12 col-md-3 mt-4 p-4 my-4">
                        <div class="card p-0 shadow">                       
                            <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : 'https://picsum.photos/200'}}" class=" p-1" alt=" ">                           
                                <div class="position-absolute secondaryBg2  rounded mt-1 ms-1">
                           <a href="{{route('announcements.show',compact('announcement'))}}" class="ms-4 button-white-Card card-link m-1 "><i class="bi bi-arrows-fullscreen"></i></a> 
                                <a href="/" class=" card-link button-white-Card m-1"> <i class="bi bi-reply"></i></a> 
                                <a href="{{route('categoryShow',['category'=>$announcement->category])}}" class="text-decoration-none card-link button-white-Card m-1 ">{{$announcement->category->name}}</a>
                            </div>
                                <div class="row p-0 ">
                            <div class="col-12 col-md-8 mx-auto items-end ">
                                <div class="card-body p-0 text-center">
                                    <h5 class="textColor">Title: {{$announcement->title}}</h5>
                                    <p class="textColor"> Price: {{$announcement->price}}</p>  
                                </div>
                                </div>
                             </div>               
                                <p class="card-foote textColor text-center"> Pubished on: {{$announcement->created_at->format('d/m/Y')}} - Author: {{$announcement->user->name ?? '' }}</p>
                        </div>
                    </div>
                    @empty
                    <div class="col-12">
                        <div class="alert alert-warning py-3 shadow">
                            <p class="lead">There are no announcements for this search, try to change article</p>
                        </div>
                    </div>
                    @endforelse
                    {{$announcements->links()}}
                </div>
            </div>
        </div>
    </div>

   
</x-layout>