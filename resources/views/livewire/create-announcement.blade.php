<div class="container-fluid">

    <div class="row   ">

        <div class="col-12 col-md-8 col-lg-6  d-flex align-items-center  ">

             <img class="img-fluid" src="../media/announcement.png" alt="">

        </div>
    <div class=" col-12 col-md-4 col-lg-6   announcements mt-4 ">
        <div class=" containerAnnouncements   py-5 px-4  ">
            <div class="d-flex justify-content-center">
                <h2>{{__('ui.createAnnouncements')}}</h2>
            </div>
            <div class="d-flex justify-content-center">
                <p>{{__('ui.underTitle')}}</p>
            </div>

    @if (session()->has('message'))
    <div class="d-flex flex-row justify-center my-2 alert alert-success ">
        {{session('message')}}
    </div>
    @endif
    <form wire:submit.prevent="store">
        @csrf
        <div class="formRegister ">
        <div class="mb-3">
            <label for="title">{{__('ui.announcementsTitle')}}</label>
             <input wire:model="title"type="text"class="form-control custom-control @error('title') is-invalid @enderror">
             @error('title')
             {{$message}}
             @enderror
        </div>

        <div class="mb-3">
            <label for="body">{{__('ui.descriptionTitle')}}</label>
             <input wire:model="body"type="text"class="form-control custom-control @error('body') is-invalid @enderror">
             @error('body')
             {{$message}}
             @enderror
        </div>

        <div class="mb-3">
            <label for="price">{{__('ui.priceTitle')}}</label>
             <input wire:model="price"type="number"class="form-control custom-control @error('price') is-invalid @enderror">
             @error('price')
             {{$message}}
             @enderror
        </div>

        <div class="mb-3">
            <label for="category">{{__('ui.categoryTitle')}}</label>
            <select wire:model.defer="category" class="form-control custom-control" id="category">
                <option value="">{{__('ui.pCategoryChoose')}}</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <input  placeholder="img" name="images" multiple class="form-control  shadow @error('temporary_images.*') is-invalid @enderror" type="file" wire:model='temporary_images'>
            @error('temporary_images.*')
            <p class="text-danger mt-2">{{$message}}</p>
            @enderror
        </div>
        @if(!empty($images))

        <div class="row ">
            <a class=" mb-3 mx-auto custom-btn-preview" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample">
                {{__('ui.preview')}} 
              </a>
            
              {{-- class="btn-close custom-btn-canva" --}}
              <div class="offcanvas offcanvas-start custom-canva-bg" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                <div class="offcanvas-header">
                  <h5 class="offcanvas-title mx-auto" id="offcanvasExampleLabel">Photo Preview</h5>
                  <button type="button" class="custom-btn-canva" data-bs-dismiss="offcanvas" aria-label="Close">

                    <i class="bi bi-x-lg "></i>
                  </button>
                </div>
                <div class="offcanvas-body">
                 

                    <div class="row custom-border-canva rounded shadow py-4 ">
                        <div class="col-12">
                        @foreach($images as $key => $image)
                        <div class="col my-3 ">
                           
                            <img class="img-fluid" src="{{$image->temporaryUrl()}}" alt="">
                            <div class="d-flex justify-content-center">
                                <button class="mx-auto custom-delete-canva shadow rounded mt-3" type="button" wire:click='removeImage({{$key}})'>{{__('ui.deleteBtn')}}</button>
                            </div>

                           
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>
              </div>
        </div>
       @endif
    </div>
    <div class="d-flex justify-content-center ">
        <button type="submit" class="btn  custom-btn  ">{{__('ui.createBtn')}}</button>
    </div>
    </form>
    </div>
</div> 
</div>
</div>


{{-- <div class="row border border-4 border-info rounded shadow py-4 ">
    <div class="col-12">
    @foreach($images as $key => $image)
    <div class="col my-3 ">
       
        <img class="img-fluid" src="{{$image->temporaryUrl()}}" alt="">
        <div class="d-flex justify-content-center">
            <button class="mx-auto shadow rounded mt-3" type="button" wire:click='removeImage({{$key}})'>Delete</button>
        </div>
        </div>
        @endforeach
    </div>
</div> --}}

{{-- PAGINA CREA ANNUNCIO ORIGINALE --}}


{{-- <div>
    <h1>Crea il tuo annuncio!</h1>
    @if (session()->has('message'))
    <div class="d-flex flex-row justify-center my-2 alert alert-success">
        {{session('message')}}
    </div>
    @endif
    <form wire:submit.prevent="store">
        @csrf
        <div class="mb-3">
            <label for="title">Announcement Title</label>
             <input wire:model="title"type="text"class="form-control @error('title') is-invalid @enderror">
             @error('title')
             {{$message}}
             @enderror
        </div>

        <div class="mb-3">
            <label for="body">Description</label>
             <input wire:model="body"type="text"class="form-control @error('body') is-invalid @enderror">
             @error('body')
             {{$message}}
             @enderror
        </div>

        <div class="mb-3">
            <label for="price">Price</label>
             <input wire:model="price"type="number"class="form-control @error('price') is-invalid @enderror">
             @error('price')
             {{$message}}
             @enderror
        </div>

        <div class="mb-3">
            <label for="category">Category</label>
            <select wire:model.defer="category" class="form-control" id="category">
                <option value="">Choose your category</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <input placeholder="img" name="images" multiple class="form-control shadow @error('temporary_images.*') is-invalid @enderror" type="file" wire:model='temporary_images'>
            @error('temporary_images.*')
            <p class="text-danger mt-2">{{$message}}</p>
            @enderror

        </div>
        @if(!empty($images))

        <div class="row">
            <div class="col-12">
                <p>Photo preview</p>
                <div class="row border border-4 border-info rounded shadow py-4">
                    @foreach($images as $key => $image)
                    <div class="col my-3">
                        <div  class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                            <button class="img-preview mx-auto shadow rounded" type="button" wire:click='removeImage({{$key}})'>Delete</button>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
       @endif

        <button type="submit" class="btn btn-primary shadow px-4 py-2">Create</button>
    </form>
</div> --}}
