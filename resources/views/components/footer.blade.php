<div class="container-fluid  mt-3 p-4 bgFooter  textFooter">
    <div class="row  mt-0 ">
        <div class=" col-12 col-md-3  mt-4 zoom  ">
            <h3 class="titleFoter fw-bold dotTitle mediaText">{{ __('ui.footerAbout') }}</h3>
            <ul class="mt-3 ulfooter setPositinUl  ">
                <li class="mt-4 mediaText ms-1 ">
                    <i class="bi bi-flag-fill mediaIcon me-2 iconFooter"></i>Evergreen Terrace
                </li>
                <li class="mt-4 mediaText ">
                    <i class="bi bi-telephone-fill mediaIcon m setPositionPH iconFooter"></i>33555-6754
                </li>
                <li class="mt-4 mediaText ms-2 ">
                    <i class="bi bi-envelope-fill mediaIcon me-2 setPositionMail iconFooter"></i>smtp.mailtrap.io
                </li>

            </ul>
        </div>
        <div class=" col-12 col-md-6 text-center  mt-4 zoom">
            <h3 class="titleFoter fw-bold dotTitle ">{{ __('ui.workUs') }}</h3>
            <p class="mt-3">Presto.it <br> {{ __('ui.register') }}</p>
            <a href="{{ route('become.revisor') }}"
                class="btn whiteBg shadow fw-bold shadow my-2">{{ __('ui.btnRevisor') }}</a>


        </div>

        <div class=" col-12 col-md-3 text-end mt-4 zoom ">
            <h3 class=" titleFoter fw-bold dotTitle mediaText">{{ __('ui.socialN') }}</h3>
            <ul class=" mt-2 ulfooter setPositinUl">
                <li class="mediaText"> <i class="bi bi-instagram me-4 iconFooter"></i>Presto_shop </li>

                <li class="mediaText mt-3"><i class="bi bi-facebook me-4 setPositionFb iconFooter"></i>Presto Outlet
                </li>
            </ul>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <p class="text-center">copyright <i class="bi bi-c-circle"></i></p>
        </div>
    </div>
</div>
