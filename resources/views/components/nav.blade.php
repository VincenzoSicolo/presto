<div id="navbar-js" class="container-fluid  miniNav-container sticky-top"> 
    <div class="row   d-flex justify-content-evenly">
        <div class="col-12 col-md-12  lightText bottom-navbar1 secondaryBg2 ">
            <div class="row p-3  mx-auto  miniNav-container ">
                
                {{-- col dropdown lenguage menu start --}}
                    <div class="col-12 col-md-12 d-flex justify-content-evenly mx-auto ">
                        <div class="custom-logo">
                            <ul class="nav custom-m-logo  me-4 ">
                                <li>
                                   {{-- <a class="nav-link " href="/"> <h4 class="  scritte_nav textOrange mt-1">Presto</h4></a> --}}
                                   <img class="img-fluid" src="../media/logo.png" alt="">
                                </li>
                            </ul>
                        </div>


                        @auth
                        @if (Auth::user()->is_revisor)
                        <div class="nav d-flex align-items-center mt-2 ">
                            {{-- <ul class="my-auto mt-2  "> --}}
                                
                                <li class="nav-item revisor-icon  ">
                                    <a  class="countSet avtive  "><h6 class="mt-2 custom-not">{{ App\Models\Announcement::toBeRevisionedCount() }}</h6></a>
                                    
                                    <a href="{{ route('revisor.index') }}" class="nav-link active navHover scritte_nav  "><h5 class="mt-2 ms-5"> Revision</h5>  
                                    
                                    </a>
                                     <a  href="{{ route('revisor.index') }}"><i class="bi bi-clipboard2-check icon_nav custom-icon  "></i></a>
                                 </li>
                            {{-- </ul> --}}
                        </div>
                            @endif
                            @endauth
                            <div class="dropdown nav my-auto">
                                <a class="nav-link active navHover scritte_nav my-auto" href="#" data-bs-toggle="dropdown" aria-expanded="false"><h5 class="custom-m-lang" class="mt-1">{{ __('ui.language') }}</h5></a>
                                <a class="custom-m-lang-icon " data-bs-toggle="dropdown" aria-expanded="false"> <i class="bi bi-globe2 lightText icon_nav "></i> </a>

                                <ul class="dropdown-menu dimension ms-4">
                                    <li class="dropdown-item ">
                                        <x-_locale lang="it" nation="it" />
                                    </li>
                                    <li class="dropdown-item">
                                        <x-_locale lang="en" nation="gb" />
                                    </li>
                                    <li class="dropdown-item">
                                        <x-_locale lang="ro" nation="ro" />
                                    </li>
                                </ul>
                            </div>


                
                {{-- col dropdown lenguage menu end --}}
                
                    
                    {{-- Nav principal links --}}
                    <div>
                        <ul class="nav my-auto">
                            <li class="nav-item mt-2">
                                <a id="scrittaHome" class="nav-link active navHover scritte_nav" aria-current="page" href="/"><h5 class="mt-1"> Home</h5></a>
                                <a href="/"><i class="bi bi-house icon_nav" href="/"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <ul class="nav my-auto">
                            <li class="nav-item mt-2">
                                <a class="nav-link navHover scritte_nav" href="{{ route('announcements.index') }}"><h5 class="mt-1">{{__('ui.announcementsN')}}</h5></a>
                                <a  href="{{ route('announcements.index') }}"><i class="bi bi-list-task icon_nav"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <ul class="nav my-auto">
                            <li class="nav-item mt-2">
                                <a class="nav-link  scritte_nav navHover" href="{{ route('categoryAnnouncement') }}"><h5 class="mt-1">{{__('ui.categoryN')}}</h5></a>
                               <a href="{{ route('categoryAnnouncement') }}"> <i class="bi bi-tag icon_nav" ></i></a>
                            </li>
                        </ul>
                    </div>
                    @guest
                    <div>
                        <ul class="nav">
                            <li class="nav-item mt-2">
                                <a class="nav-link  scritte_nav navHover " href="{{ route('login') }}"><h5 class="mt-1">Login</h5></a>
                               <a href="{{ route('login') }}"> <i class="bi bi-door-open icon_nav" ></i></a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <ul class="nav">
                            <li class="nav-item mt-2">
                                <a class="nav-link  scritte_nav navHover " href="{{ route('register') }}"><h5 class="mt-1">{{__('ui.registerN')}}</h5></a>
                                <a  href="{{ route('register') }}"><i class="bi bi-person-plus icon_nav "></i></a>
                            </li>
                        </ul>
                    </div>
                    @else
                    <div>
                        <ul class="nav my-auto">
                            <li class="nav-item mt-2">
                                <a class="nav-link navHover  scritte_nav" href="{{ route('announcements.create') }}"><h5 class="mt-1">{{__('ui.newAnnouncementsN')}}</h5></a>
                               <a  href="{{ route('announcements.create') }}"> <i class="bi bi-plus-circle icon_nav "></i></a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <ul class="nav my-auto ">
                            <li class="nav-item mt-2">
                                <a class="nav-link navHover  scritte_nav" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><h5 class="mt-1">{{__('ui.logout')}}</h5></a>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="bi bi-door-closed icon_nav"></i></a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                    @endguest
                </div>
            </div>
          </div>
        </div>
        
    </div>