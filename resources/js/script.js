
// let swiper = new Swiper(".slide-content", {
  
//     slidesPerView: 4,
//     spaceBetween: 30,
//     slidesPerGroup: 3,
//     loop: true,
//     loopFillGroupWithBlank: true,
//     pagination: {
//       el: ".swiper-pagination",
//       clickable: true,
//     },
//     navigation: {
//       nextEl: ".swiper-button-next",
//       prevEl: ".swiper-button-prev",
      
//     },
//     breakpoints: {
//       390:{
//         slidesPerView:1,
//         spaceBetween: 10,
//       },
//       640: {
//         slidesPerView: 2,
//         spaceBetween: 20,
//       },
//       768: {
//         slidesPerView: 4,
//         spaceBetween: 40,
//       },
//       1024: {
//         slidesPerView: 5,
//         spaceBetween: 50,
//       },
//     },
     
//   });
var swiper = new Swiper(".slide-content", {
    slidesPerView: 3,
    spaceBetween: 30,
    slidesPerGroup: 3,
    loop: true,
    mousewheelControl: true,
    grabCursor: true,
    keyboardControl: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    
    autoplay: {
        delay: 4000,
        disableOnInteraction: false,
      },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    
    breakpoints: {
              390:{
                 slidesPerView:1,
                 spaceBetween: 10,
               },
               640: {
                 slidesPerView: 2,
                 spaceBetween: 20,
               },
              768: {
                 slidesPerView: 3,
                spaceBetween: 40,
               },
               1024: {
                 slidesPerView: 4,
                 spaceBetween: 60,
               },
             },
  });

 

// const navbar = document.querySelector('.navbar2');

// document.addEventListener('scroll', () => {
//     let scrolled = window.scrollY;

//     if (scrolled > 150) {
//     navbar.classList.add('sticky-top')
        
//     }
// });
