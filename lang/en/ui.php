<?php

use function PHPUnit\Framework\returnSelf;

    return [
        'allAnnouncements'=>'Looking for something?',
        'introduction'=>'Looking for something?',
        'introduction2'=>'Make the best deal !',


        // WELCOME

        'welcome'=> 'Welcome to Presto !',
        'welcome-message' => 'We will help you discover the best deals online!',

        // SECTION C02

        'climate-1'=>'Reducing Co2 emission',
        'climate-2'=>'More Resources for the enviroment',
        'climate-3'=>'Buying second hand is sustainable',
        
        // CARD
        
        'price'=>'Price',
        'titolo-card'=>'Title',
        'title-carousel'=>'Explore our products',
        'category'=>'Category:',
        'view'=>'View',
        'publish'=>'Published on:',
        'author'=>'Author:',

        // FOOTER

        'footerAbout'=>'About',
        'workUs'=>'Work with us!',
        'register'=>'Register and Click',
        'btnRevisor'=>'Become a Revisor',
        'socialN'=>'Follow us',

        // NAVBAR

        'announcementsN'=> 'Announcements',
        'categoryN'=> 'Category',
        'registerN'=> 'Register',
        'newAnnouncementsN'=> 'New announcements',
        'editProfile'=> 'Edit profile',
        'logout'=> 'Logout',
        'language'=>'Language',

        // NEW ANNOUNCEMENTS

        'createAnnouncements'=> 'Create Your Announcement',
        'underTitle'=> 'Hey, Enter the required fields to create the ad.',
        'announcementsTitle'=> 'Announcement Title',
        'descriptionTitle'=> 'Description',
        'priceTitle'=> 'Price',
        'categoryTitle'=> 'Category',
        'pCategoryChoose'=> 'Choose your category',
        'selectImg'=> 'Add immage',
        'pSelectImg'=> 'No select immage',
        'createBtn'=> 'Create',
        'deleteBtn'=> 'Delete',
        'preview'=> 'View the preview',

        

        // CATEGORY

        'sport'=> 'Sport',
        'motorbike'=> 'Motorbike',
        'game'=> 'Games',
        'car'=> 'Cars',
        'informatic'=> 'Informatics',
        'informatic'=> 'Informatics',
        'electronics'=> 'Electronics',
        'fornitures'=> 'Fornitures',
        'phone'=> 'Phones',
        'house'=> 'Houses',
        
        // REVISOR

         'titleNoAnn'=> 'there are no announcement to review',
         'titleAnn'=> 'This is the announcement to review',
         'titleCard'=> 'Title',
         'descriptionCard'=> 'Description',
         'publishCard'=> 'Published on:',
         'acceptBtnCard'=> 'Accept',
         'rejectBtnCard'=> 'Reject',
         'revisorImg'=> 'Revisor image',
         'adult'=> 'Adult',
         'spoof'=> 'Spoof',
         'medicine'=> 'Medicine',
         'violence'=> 'Violence',
         'content18'=> 'Content 18+',
    ];
