<?php

use function PHPUnit\Framework\returnSelf;

    return [
        // HEADER
        'allAnnouncements'=>'Acestea sunt anunţurile noastre!',
        'introduction'=>' Cauți ceva interesant??',
        'introduction2'=>' Vă așteptăm',
          // WELCOME

          'welcome'=> 'Bine ati venit la Presto !',
          'welcome-message' => 'Vă vom ajuta să descoperiți cele mai bune oferte online!',
          // SECTION C02

          'climate-1'=>'Reducerea emisiilor de CO2',
          'climate-2'=>"Mai multe resurse pentru mediu",
          'climate-3'=>"Cumpărarea la mâna a doua este sustenabilă",

        // CARD
        'price'=>'Preț',
        'titolo-card'=>'Titlu',
        'title-carousel'=>'Explorați produsele noastre',
        'category'=>' Categorii:',
        'view'=>' Vezi',
        'publish'=>' Postat pe:',
        'author'=>'Autor:',

        // FOOTER 

        'footerAbout'=>' Contacte',
        'workUs'=>' Lucrează cu noi!!',
        'register'=>' Înregistrați-vă și faceți clic',
        'btnRevisor'=>' Deveniți recenzent',
        'socialN'=>' Urmărește-ne pe rețelele sociale',

        // NAVBAR

        'announcementsN'=> 'Toate anunţurile',
        'categoryN'=> 'Categorii',
        'registerN'=> 'Inregistreazate',
        'newAnnouncementsN'=> 'Nou anunţ',
        'editProfile'=> 'Editează profilul',
        'logout'=> 'Ieși',
        'language'=>'Limbă',

        // NEW ANNOUNCEMENTS

        'createAnnouncements'=> 'Adăugă anunț',
        'underTitle'=> 'Completați câmpurile pentru a introduce anunțul',
        'announcementsTitle'=> 'Titlul anunțului',
        'descriptionTitle'=> 'Descriere',
        'priceTitle'=> 'Preț',
        'categoryTitle'=> 'Categorii',
        'pCategoryChoose'=> 'Alege categoria ta',
        'selectImg'=> 'Adăugați imaginea',
        'pSelectImg'=> 'Niciun fișier selectat',
        'createBtn'=> 'Creați',
        'deleteBtn'=> 'Ștergeți',
        'preview'=> 'Consultați previzualizarea',


        // CATEGORY

        'sport'=> 'Sport',
        'motorbike'=> 'Motociclete',
        'game'=> 'Jocuri',
        'car'=> 'Mașini',
        'informatic'=> 'Informatică',
        'electronics'=> 'Electronică',
        'fornitures'=> 'Mobilă',
        'phone'=> 'Telefonie',
        'house'=> 'Case',

        // REVISOR

        'titleNoAnn'=> 'Nu există anunțuri de revizuit',
        'titleAnn'=> 'Acestea sunt anunțurile de revizuit',
        'titleCard'=> 'Titlu',
        'descriptionCard'=> 'Descriere',
        'publishCard'=> 'Postat pe:',
        'acceptBtnCard'=> 'Acceptă',
        'rejectBtnCard'=> 'Refuză',
        'revisorImg'=> 'Analiză imagine',
        'adult'=> 'Adult',
        'spoof'=> 'Satiră',
        'medicine'=> 'Medicină',
        'violence'=> 'Violență',
        'content18'=> 'Conținut 18+',
    ];
