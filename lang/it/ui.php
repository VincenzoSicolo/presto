<?php

use function PHPUnit\Framework\returnSelf;

    return [
        'allAnnouncements'=>'Questi sono tutti i nostri annunci!',
        'introduction'=>'Cerchi qualcosa di interessante?',
        'introduction2'=>'ti sta aspettando!',

        // WELCOME

        'welcome'=> 'Benvenuti su Presto!',
        'welcome-message' => 'Vi aiuteremo a scoprire le migliori offerte online!',
         // SECTION C02

         'climate-1'=>'Riduzione delle emissioni di CO2',
         'climate-2'=>"Più risorse per l'ambiente",
         'climate-3'=>"L'acquisto di seconda mano è sostenibile",

        // CARD 
        'price'=>'Prezzo',
        'titolo-card'=>'Titolo',
        'title-carousel'=>'Esplora i nostri prodotti',
        'category'=>'Categorie:',
        'view'=>'Vedi',
        'publish'=>'Pubblicato il:',
        'author'=>'Autore:',

        // FOOTER 

        'footerAbout'=>'Contatti',
        'workUs'=>'Lavora con noi!',
        'register'=>'Registrati e clicca',
        'btnRevisor'=>'Diventa revisore',
        'socialN'=>'Seguici nei social',

        // NAVBAR

        'announcementsN'=> 'Tutti gli annunci',
        'categoryN'=> 'Categorie',
        'registerN'=> 'Registrati',
        'newAnnouncementsN'=> 'Nuovo annuncio',
        'editProfile'=> 'Modifica Profilo',
        'logout'=> 'Esci',
        'language'=>'Lingua',

        // NEW ANNOUNCEMENTS

        'createAnnouncements'=> 'Crea il tuo annuncio',
        'underTitle'=> 'Compila i campi per inserire l\'annuncio',
        'announcementsTitle'=> 'Titolo annuncio',
        'descriptionTitle'=> 'Descrizione',
        'priceTitle'=> 'Prezzo',
        'categoryTitle'=> 'Categorie',
        'pCategoryChoose'=> 'Segli la tua categoria',
        'selectImg'=> 'Aggiungi immagine',
        'pSelectImg'=> 'Nessun file selezionato',
        'createBtn'=> 'Crea',
        'deleteBtn'=> 'Elimina',
        'preview'=> 'Vedi anteprima',


        // CATEGORY

        'sport'=> 'Sport',
        'motorbike'=> 'Moto',
        'game'=> 'Giochi',
        'car'=> 'Auto',
        'informatic'=> 'Informatica',
        'electronics'=> 'Elettronica',
        'fornitures'=> 'Arredamento',
        'phone'=> 'Telefonia',
        'house'=> 'Case',

        // REVISOR

        'titleNoAnn'=> 'Non ci sono annunci da revisionare',
        'titleAnn'=> 'Questi sono gli annunci da revisionare',
        'titleCard'=> 'Titolo',
        'descriptionCard'=> 'Descrizione',
        'publishCard'=> 'Pubblicato il:',
        'acceptBtnCard'=> 'Accetta',
        'rejectBtnCard'=> 'Rifiuta',
        'revisorImg'=> 'Revisione immagini',
        'adult'=> 'Adulto',
        'spoof'=> 'Satira',
        'medicine'=> 'Medicina',
        'violence'=> 'Violenza',
        'content18'=> 'Contenuto 18+',
    ];
