<?php

namespace App\Http\Livewire;

use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use App\Jobs\RemoveFaces;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\ResizeImage;
// use App\Models\Announcement;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateAnnouncement extends Component
{
        use WithFileUploads;

        public $title;
        public $body;
        public $validated;
        public $temporary_images;
        public $images = [];
        public $image;
        public $form_id;
        public $price;
        public $category;

        protected $rules = [

            'title'=>'required|min:4',
            'body'=>'required|min:8',
            'category'=> 'required',
            'price'=>'required|numeric',
            'images.*'=>'image|max:1024',
            'temporary_images.*'=>'image|max:1024'
        ];

        protected $messages = [
            'required'=>'The form must be minimum 4 character',
            'min'=>'The form is too short',
            'numeric'=>'The form must be a number',
            'temporary_images.*.image' => 'The image required must be image',
            'temporary_images.required' => 'The image is required',
            'temporary_images.*.max' => 'The image must be 1mb',
        ];

        public function updatedTemporaryImages()
         {
           if($this->validate([
                 'temporary_images.*'=> 'image|max:1024',
                ])){
                    foreach($this->temporary_images as $image){
                    $this->images[]=$image;
                }
           }
        }

        public function removeImage($key)
        {
            if(in_array($key, array_keys($this->images))){
                unset($this->images[$key]);

            }


        }




        public function store()
        {

            $this->validate();
            $this->announcement=Category::find($this->category)->announcements()->create($this->validate());


            if(count($this->images)){
                foreach($this->images as $image){
                    // $this->announcement->images()->create(['path'=>$image->store('images','public')]);
                    $newFileName = "announcements/{$this->announcement->id}";
                    $newImage = $this->announcement->images()->create(['path'=>$image->store($newFileName,'public')]);



                    RemoveFaces::withChain([

                             new ResizeImage($newImage->path, 400, 300),
                             new ResizeImage($newImage->path, 300, 200),
                              new ResizeImage($newImage->path, 750, 500),
                             new GoogleVisionSafeSearch($newImage->id),
                             new GoogleVisionLabelImage($newImage->id),

                    ])->dispatch($newImage->id);
                }
                File::deleteDirectory(storage_path('/app/livewire-tmp'));
            }

            $this->announcement->user()->associate(Auth::user());
            $this->announcement->save();

            session()->flash('message', 'Announcement successfully inserted, waiting for revision');
            $this -> cleanForm();



            // $category=Category::find($this->category);
            // $announcement = $category->announcements()->create([
            //     'title'=>$this->title,
            //     'body'=>$this->body,
            //     'price'=>$this->price,
            // ]);
            // Auth::user()->announcements()->save($announcement);

            session()->flash('message', 'Announcement successfully created waiting for revision');
            $this -> cleanForm();


        }

            public function updated($propertyName)
            {
                $this->validateOnly($propertyName);
            }



            public function cleanForm()
        {

            $this->title='';
            $this->body='';
            $this->category='';
            $this->images=[];
            $this->temporary_images=[];
            $this->price='';
            $this->category='';

        }

    public function render()
    {
        return view('livewire.create-announcement');
    }


}