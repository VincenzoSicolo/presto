<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Auth\User;

class MakeUserRevisor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'presto:makeUserRevisor {email}' ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make an user revisor';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->first();
        if(!$user){
            $this->error('User not found');
            return;
        }
        $user->is_revisor = true;
        $user->save();
        $this->info("The user {$user->name} it's now a Revisor");
    }
    
}
