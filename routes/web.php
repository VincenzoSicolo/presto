<?php

use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\RevisorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'welcome'])->name('welcome');

Route::get('/categorie/{category}',[FrontController::class, 'categoryShow'])->name('categoryShow');

Route::get('/new/anouncement', [AnnouncementController::class, 'createAnnouncement'])->middleware('auth')->name('announcements.create');

Route::get('/details/announcement/{announcement}',[AnnouncementController::class, 'showAnnouncement'])->name('announcements.show');

Route::get('/all/announcements',[AnnouncementController::class, 'indexAnnouncement'])->name('announcements.index');

//category page

Route::get('/category/announcements',[AnnouncementController::class, 'categoryAnnouncement'])->name('categoryAnnouncement');

//Home Revisore
Route::get('/revisor/home', [RevisorController::class, 'index'])->middleware('isRevisor')->name('revisor.index');

//Accept Announcement
Route::patch('/accept/announcement/{announcement}', [RevisorController::class, 'acceptAnnouncement'])->middleware('isRevisor')->name('revisor.accept_announcement');

//Deny Announcement
Route::patch('/reject/announcement/{announcement}', [RevisorController::class, 'rejectAnnouncement'])->middleware('isRevisor')->name('revisor.reject_announcement');

//Require to be a Revisor
Route::get('/request/revisor', [RevisorController::class, 'becomeRevisor'])->middleware('auth')->name('become.revisor');

//Require user Revisor
Route::get('/become/revisor/{user}', [RevisorController::class, 'makeRevisor'])->name('make.revisor');

//search announcement
Route::get('/search/announcement', [FrontController::class, 'searchAnnouncements'])->name('announcements.search');

//Change Language
Route::post('/language/{lang}', [FrontController::class, 'setLanguage'])->name('set_language_locale');